<center>
    <img src="./images/IDSNlogo.png" width="300" alt="cognitiveclass.ai logo"  />
</center>

# Creating Access Token

Estimated time needed: **15** minutes

## Objectives

After completing this lab you will be able to:

*   Understand how to generate a Personal Access token.



## Exercise

1. Click on **Personal Access token** hyperlink to create the access token.

![image](./images/personal_Access.png)

2. Login to your Github account.

![image](./images/Github_login.png)

3. Please provide name for the token and click on **repo** and  **gist** and click on **Generate token**.

![image](./images/new_access_1.png)

![image](./images/new_access_1a.png)

4. Copy the generate token by clicking on the copy icon.

![image](./images/copy_token.png)

5. Once the Access token is added, enter the repository name where you want to push the notebook. 


Then click on **Connect** button and you will be to see your notebook on GitHub.


![image](./images/Access_Token.png)

## Author

[Niveditha Pandith](https://www.linkedin.com/in/niveditha-pandith-53a057231/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDeveloperSkillsNetworkDB0201ENSkillsNetwork20127838-2022-02-08)




## Change Log

| Date (YYYY-MM-DD) | Version | Changed By        | Change Description                        |
| ----------------- | ------- | ----------------- | ----------------------------------------- |
| 2022-05-09        |  0.2    | Malika            | Updated or added images as per the UI |
| 2022-05-05        |  0.1    | Niveditha         | Created Initial Version.            |


<hr>

## <h3 align="center"> © IBM Corporation 2022. All rights reserved. <h3/>
